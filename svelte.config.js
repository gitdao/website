// See https://github.com/sveltejs/svelte-preprocess for more information about preprocessors

import { mdsvex } from "mdsvex";
import rehypeAutolinkHeadings from "rehype-autolink-headings";
import rehypeSlug from "rehype-slug";
import remarkGfm from "remark-gfm";
import remarkToc from "remark-toc";
import image from "svelte-image";
import preprocess from "svelte-preprocess"
//import adapter from "@sveltejs/adapter-auto"
import adapter from "@sveltejs/adapter-static";

/** @type {import("@sveltejs/kit").Config} */
const config = {
  extensions: [
    ".md",
    ".svelte",
    ".svx",
  ],
  preprocess: [
    preprocess({
      postcss: true,
    }),
    mdsvex({
      extensions: [
        ".md", // Enable better sntaxing coloring as of 2022-02-02
        ".svx",
      ],
      layout: {
        blog: "src/routes/blog/_layout_post.svelte"
      },
      // `mdsvex` has a simple pipeline. Your source file is first parsed into a Markdown AST (MDAST), this is where `remark` plugins would run.
      // Then it is converted into an HTML AST (HAST), this is where `rehype` plugins would be run.
      // After this it is converted (stringified) into a valid Svelte component ready to be compiled.
      rehypePlugins: [ // Plugins that affects the HTML
        rehypeSlug, // Needs to appear before `rehypeAutolinkHeadings`
        rehypeAutolinkHeadings,// Configuration options for rehype-auto-link-headings
      ],
      remarkPlugins: [ // Plugins that affects the Markdown
        remarkGfm, // See https://github.com/remarkjs/remark-gfm
        remarkToc, // See https://github.com/remarkjs/remark-toc
      ],
    }),
    image(),
  ],
  kit: {
    adapter: adapter(),
    prerender: {
      default: true,
    },
  },
}

export default config
