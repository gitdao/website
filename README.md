# website

A website for GitDao

## Commands

In a nix-enabled environment, if you have `direnv`, run `touch flake.nix`; otherwise use `nix develop`.
This will configure the environment with all the required tooling.

Now:

- `deploy`: Cleans everything, then builds a production ready version of the website then upload the website to IPFS.
  You will still need to update the Unstoppable Domain by hand as automating this was not possible.

- `pnpm update`: Upgrade NPM dependencies in `packages.json`.

- `pnpm dev`: Runs the development server.

- `build`: Build the production version of the website.
  The production-ready website will be stored in `build/`.

- `upload-to-ipfs`: Uploads the content of `build/` to IPFS, i.e. pins it on https://pinata.cloud.

## Deployments

### IPFS

The official, but complex, way of deploying the website.
Use the `deploy` command.

### Heroku

The unofficial, quick and dirty way to deploy the wbesite.

```sh
git push heroku <branch_to_deploy>:main # main is the heroku branch, not the local branch name 👌
```

## Design Inspiration

- https://replit.com/
- TabNine
- uniswap.org

## Installation Template

https://linu.us/sveltekit-with-tailwindcss-v3

## Instructions Followed

- Building a blog in SvelteKit: https://joshcollinsworth.com/blog/build-static-sveltekit-markdown-blog
