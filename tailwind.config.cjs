const colors = require("tailwindcss/colors")
const plugin = require("tailwindcss/plugin")

const config = {
  content: ["./src/**/*.{html,js,svelte,ts}"],
  darkMode: "class",
  mode: "jit",
  plugins: [
    require("@tailwindcss/typography"),
    require("@tailwindcss/forms"),
  ],
  theme: {
    colors: {
      transparent: "transparent",
      current: "currentColor",
      black: colors.black,
      white: colors.white,
      blue: colors.blue,
      gray: colors.neutral, // colors.slate
      green: colors.emerald,
      pink: colors.fuchsia,
      purple: colors.violet,
      red: colors.red,
      yellow: colors.amber,
      orange: colors.orange
    },
    boxShadow: {
      "md": "0 4px 25px 10px rgba(0, 0, 0, 0.07)",
    },
    fontFamily: {
      serif: ["Andada Pro", "serif"],
      sans: ["Lato", "sans"]
    },
    extend: {},
  },
}

module.exports = config
