{
  description = "A website for GitDAO";

  inputs = {
    devshell.url = "github:numtide/devshell";
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  };

  outputs = { self, devshell, flake-utils, nixpkgs }:
    flake-utils.lib.eachDefaultSystem (system: 
      let

        pkgs = import nixpkgs {
            inherit system;
            overlays = [ devshell.overlay ];
        };

        packageJSON = ./package.json;
        package = pkgs.lib.importJSON packageJSON;

        pname = package.name;
        version = package.version;

        src = builtins.path {
          path = ./.;
          name = pname;
        };
      in {

        devShell = # Used by `nix develop`, value needs to be a derivation
          pkgs.devshell.mkShell {

            name = "TheGitDAO Website";

            commands = [
              # Custom Commands
              {
                name = "build";
                help = "Builds the website.";
                category = "CI";
                command = let
                  build = pkgs.writeScript "ci-build" ''
                    pnpm i --frozen-lockfile
                    pnpm build
                  '';
                in "${build}";
              }
              {
                name = "upload-to-ipfs";
                help = "Uploads the compiled website to IPFS.";
                category = "CI";
                command = let
                  upload-to-ipfs = pkgs.writeShellApplication {
                    name = "upload-to-ipfs";
                    runtimeInputs = with pkgs; [
                      curl
                      ipfs
                      killall
                    ];
                    text = ''
                      set -x

                      STARTED_IPFS="false"

                      function cleanup {
                        # Only kill IPFS if it was started by this script
                        if [[ "$STARTED_IPFS" == "true" ]]; then
                          killall -q ipfs
                        fi
                      }
                      trap cleanup EXIT

                      # Check that the website is already built
                      if [ ! -d "./build" ]; then
                        echo "The website cannot be uploaded because it is not built. To build the website, run \`nix run \".#build\"\`."
                        exit 1
                      fi

                      # Initialize IPFS if not already done
                      if [ ! -d "$HOME/.ipfs" ]; then
                        echo "Initializing uninitialized IPFS"
                        ipfs init
                      fi

                      # Start IPFS daemon if not already running
                      if ! pgrep -f "ipfs daemon"; then
                        echo "Starting IPFS deamon"
                        ipfs daemon &
                        STARTED_IPFS="true"
                      fi
                      
                      # Wait until IPFS is running
                      ATTEMPT_COUNTER=0
                      MAX_ATTEMPTS=10
                      until curl --output /dev/null --silent --head --fail "http://127.0.0.1:5001/webui"; do
                        ATTEMPT_COUNTER=$(( ATTEMPT_COUNTER + 1 ))
                        if [ "$ATTEMPT_COUNTER" -eq "$MAX_ATTEMPTS" ];then
                          echo "Max attempts reached. Could not connect to IPFS daemon. Exiting now."
                          exit 1
                        fi
                        sleep 1
                      done

                      # Upload content to Pinata
                      if ! ipfs pin remote service ls | grep "pinata"; then
                        ipfs pin remote service add pinata "https://api.pinata.cloud/psa" "$PINATA_JWT"
                      fi
                      # Using CID v0, and not CID v1, because Unstoppable does not support CID v1 as of 2022-01-31
                      CID=$(ipfs add --recursive --quiet --cid-version 0 "./build" | tail -1)
                      ipfs pin remote add --service=pinata --name="${pname}-${package.version}" "$CID"

                      echo ""
                      echo -e "Please update the Unstoppable Domains IPFS hash (https://unstoppabledomains.com/manage?page=website&subpage=websiteLinkingForm&domain=the-git-dao.crypto) with:\n\t$CID"
                      echo ""

                      # TODO Update IPFS CID on Unstoppable domains
                    '';
                  };
                in "${upload-to-ipfs}/bin/upload-to-ipfs";
              }
              {
                name = "deploy";
                help = "Deploys the website.";
                category = "CI";
                command = let
                  deploy = pkgs.writeScript "deploy" ''
                    pnpm clean
                    build
                    upload-to-ipfs
                  '';
                in "${deploy}";
              }
              {
                name = "deploy-to-heroku";
                help = "Deploys the website to heroku.";
                category = "CI";
                command = let
                  deploy-to-heroku = pkgs.writeShellApplication {
                    name = "deploy-to-heroku";
                    runtimeInputs = with pkgs; [
                      git
                      heroku
                    ];
                    # Setup the git repo with `heroku git:remote -a the-git-dao`.
                    # You will need to commit to git whatever you want to upload to heroku.
                    # Heroku will build the app using npm build, then run the app using npm start.
                    text = ''
                      git push heroku "$(git rev-parse --abbrev-ref HEAD)":main
                    '';
                  };
                in "${deploy-to-heroku}/bin/deploy-to-heroku";
              }
              # Package Imports
              {
                category = "javascript";
                package = pkgs.nodePackages.pnpm;
              }
              {
                category = "CI";
                package = pkgs.heroku;
              }
            ];

            packages = with pkgs; [
              ipfs
              nodejs
            ];
          };
      }
  );
}