---
title: Blockchain Voting
date: 2022-02-15
categories:
- voting
---

From <https://medium.com/daostack/voting-options-in-daos-b86e5c69a3e3>.

Blockchain voting is mostly used by DAOs.
Most DAOs also manage some funds.
Relative majority systems where you simply compare the number of votes in favor and against are not a viable alternative because some ill-intended actor could easily empty the funds of the DAO while others aren't looking.

Requiring in a DAO too much of the collective attention to be spent on and approve each and every decision makes a decentralized governance system obviously unscalable; while requiring too little potentially makes it not resilient to faulty decisions, collusion, or simply misrepresentation of the collective opinion.

## Liquid Democracy

See <https://en.wikipedia.org/wiki/Liquid_democracy>.

People can either vote directly or delegate their vote to someone else that will vote on their behalf.
Any individual may be delegated votes (those delegated votes are termed "proxies") and these proxies may in turn delegate their vote as well as any votes they have been delegated by others resulting in "metadelegation".

This delegation of votes may be absolute (an individual divests their vote to someone else across all issues), policy-specific (an individual divests their vote to someone only when the vote concerns a certain issue), time-sensitive (an individual decides to divest their vote for a period of time).
In the case of absolute delegation, the voter situates themselves as a participant in a representative democracy, however, they have the right to revoke their vote delegation at any time.

## Staking Voting Systems

A form of voting system that requires token.

Staking some token
: Locking the token for some given amount of time in a smart contract.

Give a voting power proportional to the amount of token that you lock and to the time you lock the tokens.
Prevents some kinds of attacks.

## Quorum Based Voting

Quorum
: A minimum threshold that needs to be achieved.
The quorum is often specified using relative values, i.e. percentages.
The units of the quorum value are often people or votes, i.e. a minimum number of people need to take part to the vote for it to be valid or a minimum percentage of positive votes needs to be achieved for the voted proposal to pass.

Quorum based voting often imposes a quorum on the participation rate in order for a proposal to be accepted, e.g. at least 60% of voters need to vote for the outcome of the vote to be applied.
Some voting systems have quorums on the positive votes, e.g. a 20% quorom on positive votes would imply that at least 20% of the voters vote positively for any proposal to be considered.

This system has been used in practice very often in political systems.

Token-based voting systems have the downside of being akin to plutocracies (the person that has the more tokens, i.e. the person that has the more money, has the most power).
In such systems, finding the right quorums is hard.
If the quorum is too low, the system is vulnerable to attacks from malicious agents.
If the quorum is too high, then it is very hard to get anything accepted.
This is called a governance lock.

This system also suffers from strategic voting.

### Positive Remarks from Real-Life Quorum-Based Voting Systems

Turns into a whale-chasing game for the people making proposals.

Not a problem to reach the quorum because whales are active.

### Negative Remarks from Real-Life Quorum-Based Voting Systems

Sometimes an issue to reach the quorum.

Fosters strategic voting.

On Ethereum, voting has a financial cost and there is speculation around the governance token as those tokens have value.
**Would prefer that financial aspects and governance aspects be fully separated.**

## Holographic Consensus

Spearheaded by DAOstack.

Fundamentally a process to increase voting efficiency in a community.
Through some tokenomics, the system tries to filter the proposals that are the most interesting so that voters with little time can spend it on the most promising proposals.
Allow for decisions in a DAO to be made locally —i.e. with limited attention and voting power, as long as these decisions are ensured to be in line with the global opinion of the DAO.
We’ve coined this peculiar situation holographic consensus, being reminiscent of a hologram where every little piece of the picture actually contains the information of the entire image.

Holographic Consensus
: Associates a prediction market with each proposal.
Predictors can stake funds for or against a proposal they believe will pass or fail.
If they predict correctly, they benefit financially.
Proposals that are predicted to pass are “boosted” and voting switches from 50% *quorum* to relative majority, i.e. no quorum needed anymore, you only look at For vs. Against votes.
This makes the barrier to pass proposals predicted to pass much lower than proposals that don’t have funds staked on them.

Projects that have used this voting mechanism include DXdao, NecDAO and Prime DAO.

This voting mechanism naturally protects projects from nefarious proposals, since you need to pay in order to attack (via staking funds on proposals).
It works great for projects that have many proposals since members only really need to pay attention to proposals with a stake on them.
It also allows a DAO to pass proposals quickly.

On the flipside, the UX is more confusing and it introduces a new token mechanism (staking on proposals) which isn’t always desired.

### Positive Feedbacks on HC

*Works well, even though the scale at which HC becomes useful was never reached so far.*

### Negative Feedbacks on HC

Adds complexity to the voting mechanism and a second token (the one for predicting the outcome of the vote).
This means that people need more time to be familiar with the voting system.

Complexity of the system also causes some high gas fees.

I believe HC can work well for filtering proposals for a DAO.
I’m not really sure it works well for making the best decisions for a DAO though, since people with specific knowledge about something aren’t necessarily the same people who have voting power.

Would switch to having multiple voting mechanisms for different decisions. HC for base layer decisions, then Conviction Voting for budgetary decisions. 1 Token 1 Vote for other things. To me, it makes the most sense to use the right tool for the right thing. HC makes sense for high level, but not necessarily low level.

## Permissioned Relative Majority (MolochDAO)

A relative majority system (you simply count the number of votes in favor and the ones against the proposal) where proposals need to be sponsored by DAO members in order to be voted on.
By requiring that proposals are sponsored by a member of the DAO, you provide some additional guarantees against attack against the DAO, e.g. someone trying to take all the funds of the DAO.

Voters need to dedicate less attention to the voting system.

However, the system is slow because someone needs to review each proposal.
Further, the system is only as secure as the people that can review proposals.

### Positive Feedback on Permissioned Relative Majority

It flips the burden from “get enough people to say yes” for quorum-based voting, to “it’s your job to say ‘no’ if you care.”

Simple

### Negative Feedbacks on Permissioned Relative Majority

Slow.

Might want to add fluid democracy.

Requires reviewer to be active.

Proposals can be accepted with only one vote, so the reviewer community really need to be cautious.

Want to add quorum-based voting on top of permissioned relative voting so that some proposals can be accepted immediately.

## Conviction Voting

Conviction voting is a voting mechanism in which people stake their voting power on proposals, and over time, those proposals accumulate enough votes to pass.

## Condorcet Method Voting Systems

## Quadratic Voting

## Arrows Impossibility Theorem

 According to Arrow's impossibility theorem, in all cases where preferences are ranked, it is impossible to formulate a social ordering without violating one of the following conditions:

- Nondictatorship: The wishes of multiple voters should be taken into consideration.
- Pareto Efficiency: Unanimous individual preferences must be respected: If every voter prefers candidate A over candidate B, candidate A should win.
- Independence of Irrelevant Alternatives: If a choice is removed, then the others' order should not change: If candidate A ranks ahead of candidate B, candidate A should still be ahead of candidate B, even if a third candidate, candidate C, is removed from participation.
- Unrestricted Domain: Voting must account for all individual preferences.
- Social Ordering: Each individual should be able to order the choices in any way and indicate ties.
