---
title: Progress Week 8
date: 2022-03-15
categories:
- progress-report
---

## ToDo

- [ ] Build MVP:
  - [ ] Quadratic voting
  - [ ] Donations splitting to three buckets: developpers, drips, stash of the DAO
  - [ ] Build a centralized registry that GitDAO register to (enable building a reputation system)
- [ ] Annonce myself to the Radicle project
- [ ] Write tests for smart contracts
  - [x] `NonTransferableERC721`
  - [x] `TimeLimitedGovernanceToken`
  - [x] `LinearlyTimeLimitedGovernanceToken`
  - [ ] `QuadraticVoting`
