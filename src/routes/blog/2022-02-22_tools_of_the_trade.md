---
title: Tools of the Trade for Blockchain Development
date: 2022-02-22
categories:
- tooling
- ethereum
---

## Ether.js

An alternative to web3.js.
The most common library for Ethereum applications on the web.

## Ganache

Emulates an Ethereum blockchain on a computer to make development easier.

## Infura

Infrastructure-as-a-Service for Ethereum.

## Solidity

High-level programming language for smart contracts.
Compiles to Ethereum Byte-code.

## Truffle

- Contract compilation and deployment.
- Automated contract code testing.
- An interactive console to work with the built contracts.
- An external script runner that works with the contracts that are included.
