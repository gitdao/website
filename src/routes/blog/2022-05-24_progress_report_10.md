---
title: Progress Report 10
date: 2022-05-24
categories:
- progress-report
---

## Done

### 2022-05-25

- Read Twenty Lectures on Algorithmic Game Theory from Tim Roughgarden
  - Read chapter six
    Interesting idea: The payment rule charges each agent her externality—the welfare loss inflicted on the other agents by her presence.
  - Skimmed chapter seven
  - Read chapter eight on spectrum auctions, and why they are run as simultaneous ascending auctions (SAA).
    Interesting ideas:
    - Indirect mechanisms learn information about bidders’ preferences only on a need-to-know basis.
    - The preferred method in practice of selling items separately is simultaneous ascending auctions (SAAs).
    - SAAs are vulnerable to demand reduction, where a bidder reduces the number of items requested to depress the final selling prices.
    - When items can be complements, SAAs also suffer from the exposure problem, where a bidder that desires a bundle of items runs the risk of acquiring only a useless subset of them.
  - Read chapter nine.
    - The Top Trading Cycle (TTC) algorithm is a method for reallocating objects owned by agents (one per agent) to make the agents as well off as possible using trading cycles in a graph.
  - Read chapter ten.
    - Kidney exchange: cannot use TTC, use maximum cardinality matchings instead.
    - Hospitals might have an incentive not to report some of their patients to maximize the number of *their* patients that they match.
    - Deferred acceptance algorithm: to match vertices belonging to two distinct set with equal cardinality, where each vertex has a complete preference ordering over the vertices from the other set.
    - Deferred acceptance algorithm has many nice properties: DSIC for the vertices that propose, some notion of optimality on the achieved matching, etc.
  - Read chapter eleven.
    - There are many game with no dominant strategies, i.e. the game is such that you must adapt your strategy to the behavior of the other participants.
      In such case, it makes no sense to talk about DSIC.
      The Price of Anarchy is then an interesting metric.
    - Selfish routing: PoA is unbounded when there are highly non-linear cost functions.
    - PoA remains small if there are no cost functions that are too highly non-linear.

### 2022-05-27

- Read Twenty Lectures on Algorithmic Game Theory from Tim Roughgarden
  - Read chapter twelve.
    - Selfish routing system and some more bounds on the Price of Anarchy
    - Atomic selfish routing, maximum PoA in such settings (2.5) and the existence of multiple nash equilibria with different costs.

### 2022-05-28

- Read Twenty Lectures on Algorithmic Game Theory from Tim Roughgarden
  - Read chapter thirteen
    - Extended notions of Equilibria like Correlated Equilibria.
    - Computational tractability of such relaxed equilibria.

### 2022-05-30

- Read Twenty Lectures on Algorithmic Game Theory from Tim Roughgarden
  - Skimmed chapter fourteen.
    - Smooth games
    - PoA bounds for smooth games.
  - Read chapter fiftheen.
    - Network cost-sharing games.
    - Price of Stability.
    - Strong Nash Equilibria.
  - Read chapter sixteen.
    - Player dynamics.
    - Better-response dynamic in potential games converge to PNE.

### 2022-05-31

- Read Twenty Lectures on Algorithmic Game Theory from Tim Roughgarden
  - Skimmed chapter seventeen
    - No-regret dynamics.
  - Decided not to read chapter 18 to 20 as they are less relevant for me.
- Read Arrow's impossibility theorem again.
- Read Gibbard's theorem (basically the same as Arrow's theorem)
- Read https://medium.com/ethereum-optimism/retroactive-public-goods-funding-33c9b7d00f0c
- Read "Notes on Blockchain Governance" by Vitalik Buterin (https://vitalik.ca/general/2017/12/17/voting.html)

### 2022-06-06

- Read "Coordination Failures" by Vitalik Butterin (https://vitalik.ca/general/2017/05/08/coordination_problems.html).
- Read "Call for Moratorium on the DAO" (https://hackingdistributed.com/2016/05/27/dao-call-for-moratorium/). See notes.
- Read "Bitshares is Trying What MemoryCoin Did a Year Ago (to Disastrous Ends)" https://btcgeek.com/bitshares-trying-memorycoin-year-ago-disastrous-ends/. Useless.
- Read "Complexity of Value" by Lesswrong (https://www.lesswrong.com/tag/complexity-of-value).
- Read "Bitshares is Trying What MemoryCoin Did a Year Ago (to Disastrous Ends)" https://slatestarcodex.com/2017/11/21/contra-robinson-on-public-food/. Useless.
- Read "Review of Optimism retro funding round 1" by Vitalik Butterin (https://vitalik.ca/general/2021/11/16/retro1.html).

## TODO

- Look into quadratic voting!
