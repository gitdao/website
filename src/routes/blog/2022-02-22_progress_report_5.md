---
title: Progress Week 5
date: 2022-02-22
categories:
- progress-report
---

## ToDo

- [ ] OPTIONAL Have a look at the working of Uniswap v3 LP NFTs that are probably somewhat similar to what I want to build.
- [ ] Start implementing self-destructing token.
- [ ] Read at least parts of the Solidity language documentation.
- [ ] Implement Radicle Anchor in my smart contract.
- [x] Introduce myself to the DAO Hackathon community.
- [x] Enroll into DAO Hackathon.
- [x] Reverse engineered how Anchor works at the smart contract level.
- [x] Read Drips documentation.
- [x] Read an article on [Open Source funding through DAOs](https://kyledenhartog.com/sustainable-OS-dev-daos/).
- [x] Read the [multihash documentation](https://multiformats-io.ipns.dweb.link/multihash/)
- [x] Deployed the website on Unstoppable Domains
- [x] Improved the navbar slightly (fixed the logo)

## MVP

- Basic voting system
- Constant value rewarding scheme
- Value of NFT decreases in time either with the step function or as an exponential
- KISS for now!

## Various Feature Ideas

- Refresh contributions? I.e. Governance token don't loose their value if you contribute again -> Incentivizes poor quality contributions
- NFT value decrease over time. Exponentially, step function, linearly?
- NFT cannot be destructed, good thing: owning an NFT proves contribution -> Open Source reputation system
- Incentivize early contributions more somehow
- Include other metrics in the NFT like code quality -> Incentivezes good quality code.
- Consider what happens if the project is abandoned

Vocabulary ideas:

- Stash, Vault
- Self-healing system

Rewarding Scheme:

- Token value proportional to the number of people contributing
