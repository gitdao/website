---
title: Notes on Encode's DAO Hackathon
date: 2022-02-15
categories:
- DAO
- hackathon
---

## Challenges Listed on DAO Hackathon

DAO Tooling Challenges

- Web3 communications (Discord)
- Engage DAO communities (better version of Forum)
- Improve over GNOSIS SAFE
- Access control based on token ownership (e.g. collab.land)