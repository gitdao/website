---
title: Progress Week 4
date: 2022-02-15
categories:
- progress-report
---

## ToDo

- [ ] Start building DAO smart contract
- [ ] Introduce myself to the Radicle community
- [ ] OPTIONAL Complete notes on voting systems

- [x] Define list of features for my DAO -> self-destructing tokens
- [x] Read some more on Radicle
- [x] Find existing DAO smart contracts that interact with Radicle
- [x] Fix website pipeline
- [x] Setup development environment (https://github.com/johnnylambada/howto-eth/blob/article00/articles/00-Deploy-a-Fully-Tested-NFT-Contract-Using-OpenZeppelin/article.md)
- [x] Create repo with code on Radicle.
- [x] Read [article](https://javascript.developpez.com/actu/330193/Faker-js-la-bibliotheque-intentionnellement-corrompue-par-le-developpeur-est-maintenant-un-projet-controle-par-la-communaute-et-son-developpement-sera-assure-par-une-equipe-de-8-personnes/) on `faker.js`.

## Post-Poned

- [ ] Enroll into DAO Hackathon
