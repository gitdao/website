---
title: Progress Week 6
date: 2022-03-01
categories:
- progress-report
---

## ToDo

- [ ] Start implementation of TheGitDAO smart contract
- [ ] Read [tutorial](https://learn.figment.io/tutorials/build-a-dao-on-celo) on creating DAO smart contrac
- [ ] Read some Open Zeppelin documentation, what are useful smart contract to inherit from for example

- [x] DAO Hackathon Reached out to people, talked to some of them, found a few teammates.
- [x] Created a [Discord server](https://discord.gg/UM5tJ8nK) and added the link to the home page of [the-git-dao.crypto](https://the-git-dao.crypto).
- [x] Learning Solidity: Finished Lessons 1 to 9 of [CryptoZombie](https://cryptozombies.io/en/course)

## Post-Poned

- [ ] Read book/documentation on Ethereum programming
- [ ] Read Truffle documentation?

## Feature Ideas MVP

- Quadratic voting
- Smart contract for Governance token ERC721 -> Reputation system, time-limited?
- Commit hash

- Message Andy when working implementation of Governance token
