---
title: Progress Week 7
date: 2022-03-08
categories:
- progress-report
---

## ToDo

- [x] Advertise Git repos on the website
- [ ] Build MVP:
  - [x] ERC721 time-limited governance tokens
  - [x] Interface with anchor storage
  - [ ] Quadratic voting
- [ ] Annonce myself to the Radicle project
- [x] Install Radicle cli, update Radicle upstream
- [x] Enroll myself officially for the master project

## Food for Thoughts

- Centralized registry for governance tokens or each DAO keeps its own token? Debate pros and cons.
- Deploy as soon as possible to Dragonfly testnet -> Make the contract destroyable

## Tools

Hardhat: Decided to use hardhat instead of Truffle: the web seems to generally agree that it provides a better experience.

Solidity: Reinforced my opinion that using Solidity is better than Vyper after reading a couple of article on the topic.
