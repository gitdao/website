---
title: Progress Week 3
date: 2022-02-08
categories:
- progress-report
---

## ToDo

- [ ] Other resources on DAOs like posts from Vitalik Butterin.
- [ ] Switch from using Andada semibold to Andada bold to increase contract between normal and bold.
- [ ] OPTIONAL Fix GitLab pipeline

- [x] OPTIONAL Improve GitLab pipeline to only upload code to IPFS/Pinata when creating a new version through a `git` tag
- [x] Read about Radicle
- [x] Look into existing DAOs and list features pertinent for my DAO
- [x] Setup Development tools (Truffle, Ganache, etc.)

## Ideas

- Use [Radicle](https://radicle.xyz)
- Take part into [Encode's DAO Hackathon](https://medium.com/encode-club/announcing-the-encode-x-dao-hack-81e884f63cee), starts on March 2nd, lasts for four weeks

## Website

- Tested website from Brave app on my phone without VPN.
  Also not working...
- Reached out by email to Unstoppable Domain to know why I cannot esolve the DNS name in any browser.
- Created the [ideas](/ideas) page
- Created the [decisions](/decisions) page.
- Discovered that my build pipeline is not working anymore (already, after only one week)
  Tried to fix the pipeline in various ways:

  - Use `npmlock2nix`
  - Use flake `apps`
  - In the end, I wanted to build using a `pnpm` docker container directly, but docker hub seems down...
- Read about DAOs and took notes
- Discovered DAO Hackathon from Encode and read about it
- Discovered [radicle](https://radicle.xyz) and read most of its doc
- Took notes on voting systems
