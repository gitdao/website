---
title: Exploratory Deep-Dive in Open Source
date: 2022-02-04
categories:
- open-source
- exploration
---

## Table of Content

## Context

Aspects of interest:

- Governance process (Merge Requests)
- Ownership and ownership distribution
- DAO membership types
- Tools for donations
- Tools for distributing donations down to other projects
- Handovers

## Properties of Interest

- Prevent fatigue: keep the processes as slim and flexible as possible.

## Ideas from Readings

- Different types of DAO members? Couter-argument: SNAFU.
- Remuneration for PR proportional to perceived quality.
- Different regimes for different project sizes.
- Propose two release strategies by default: `stable` (fixed time-intervals, only mature features) and `cutting-edge` (release immediately features when they are done).
- Keep track of each developer's reputation -> Developer selection.
- Efficient communication tools.
- Start from individual vision, then expand project through community

- OS traditional incentives: creative work, recognition from peers (reputation).

## Sources

- [x] The Cathedral and the Bazaar, Eric S. Raymond
- [ ] The Magic Cauldron
- [ ] Homesteading the Noosphere
- [ ] Python
- [ ] Faker.js (https://javascript.developpez.com/actu/330193/Faker-js-la-bibliotheque-intentionnellement-corrompue-par-le-developpeur-est-maintenant-un-projet-controle-par-la-communaute-et-son-developpement-sera-assure-par-une-equipe-de-8-personnes/)
- [ ] Unix kernel as a counter example?

## The Cathedral and the Bazaar

Linux, even though a highly complex and vast project, is built using a Bazaar organization style.
In essence: even very large projects can be built using fully decentralized, almost anarchic, organization processes.

### General

In the real world, efforts are not rewarded, only results are.

The Delphi effect: The averaged opinion of a mass of equally expert (or equally ignorant) observers is a more reliable predictor than the opinion of a single randomly-chosen observers.

Hypothesis on the three keys of OS's success:

- No deadline policy, equivalently named "Wake me up when it's done" scheduling policy
- Process transparency
- Developer self-selection

### Good Solution

You often don't really understand the problem until after the first time you implement a solution.
Plan to throw one solution away, you will anyhow.

The next best thing to having good ideas is recognizing good ideas from your users.
Sometimes the latter is better.
It is not critical that the project coordinator can originate brilliant designs.
It is absolutely critical that the project coordinator be able to recognize brilliant ideas from others.

Often, the most striking and innovative solutions come from realizing that your concept of the problem was wrong.

### Leadership / Project Organization

Consider the way ants find food: exploration essentially by diffusion, followed by exploitation mediated by a scalable communication mechanism.

A bazaar project coordinator must have good people and communications skills.
People who want to lead collaborative projects have to learn how to recruit and energize effective communities of interest.

The developer who uses only his or her own brain in a closed project is going to fall behind the developer who knows how to create an open, evolutionary context in which feedback exploring the design space, code contributions, bug-spotting, and other improvements come from from hundreds of people.

The OS world behaves in many respects like a free market or an ecology, a collection of selfish agents attempting to maximize utility which in the process produces a self-correcting spontaneous order more elaborate and efficient than any amount of central planning could have achieved.
The OS devs are maximizing is not classically economic, but is the intangible of their own ego satisfaction and reputation.

The future of open-source software will increasingly belong to people who start from individual vision and brilliance, then amplify it through the effective construction of voluntary communities of interest.

OS needs to align the selfish incentive with the benefit of the project.

Three size regimes for software projects:

- `small`: one to three developers.
  No more organization is required than potentially choosing a leader.
- `intermediate`: traditional mamangement has the most benefits at this scale because the overhead remains low and the benefits from preventing duplicate work is high.
- `large`: At this point, traditional management costs more than it bring and a bazaar-style management is best.

### Bootstraping

One cannot code from the ground up in bazaar style.
Go public when you have something runnable and testable to play with, i.e. a plausible promise of what the future can be.

### Handovers

When you lose interest in a program, your last duty to it is to hand it off to a competent successor.

### Users / Co-Developers

Good to have some:

- Shows that you are creating some real value.
- Users can become co-developers.
  Treating your users as co-developers is your least-hassle route to rapid code improvement and effective debugging.

Listen to your users.

Contributions are received from people who are interested enough to use the software, learn how it works, find solutions, and actually produce a feature/fix.
Anyone who passes all these filters is highly likely to have something useful to contribute.

Brooks's Law: The complexity and communication costs of a project rise with the square of the number of developers, while work done only rises linearly.
Communications/coordination overhead on a project tends to rise with the number of interfaces between human beings.

Define three developers type:

- `coordinator`: Coordinates work across the project.
- `core`: Do the coding that requires a lot of knowledge of the project/a lot of collaboration with the other developers.
- `halo`: work on separable parallel subtasks and interact with each other very little.

Only the core pays the full Brooksian overhead.
Is this a way to circumvent Brook's law?

Grow your user list aggresively.

Programmer time is not fungible; adding developers to a late software project makes it later.

### Releases

Release Early, Release Often.
Even if buggy.

Use a two-release-type strategy:

- `stable`
- `cutting-edge`

It makes each release more attractive.

Send chatty announcements to your users whenever you release, encourage people to participate.

Immutable feature list + immutable deadline -> bad quality code and a mess.
You need to relax at least one requirement.
Releasing features whenever they are ready is equivalent to the cutting-edge release strategy.
Releasing at fixed time-intervals, but only the features that are ready, is equivalent to the stable release strategy.

The cutting-edge release strategy, which could also be named "wake me up when it's done" release strategy produces the best quality code, and shorter delivery times on average.

The anti-deadline policy of OS is probably one of its key success factor.

### Bugs

Your code will always be buggy.
Accept it and release early.

Linus's Law: Given enough eyeballs, all bugs are shallow.

Debugging is parallelizable, although it requires debuggers to communicate with some coordinating developer.

Maintaining a widely used program is typically 40 percent or more of the cost of developing it.

Bug reports from non-source-aware users is generally useless as they report surface symptoms and take their environment for granted.
Therefor they omit background information and seldom include a recipe for reproducing the bug.

Problems scale with the number of communications paths between developers, i.a. as the square of the humber of developers.

With rapid development cycles, development and enhancement may become special cases of debugging, i.e. fixing 'bugs of omission'.

Distribution of trace difficulty, i.e. how complex it is to understand a bug from a trace or symptoms list, would suggest that even solo developers should emulate the bazaar strategy by bounding the time they spend on tracing a given symptom before they switch to another. Persistence may not always be a virtue...

### Good Code

Smart data structures and dumb code works a lot better than
the other way around.

Cultivate excellence.

### Rewards / Motivation

Linus Torvald was keeping his users constantly rewarded-stimulated by the prospect of having an ego-satisfying piece of the action.

Solving interesting problems.

Doing creative work.

Flow theory: people have fun when tasks are challenging enough not to be boring but easy enough as to not be impossible.
Joy is probably the biggest motivator in OS.
Play is the most economically efficient mode of creative work.

### True Innovation

Innovation come from the individual.
The management model has no impact on how much innovation can happen.
It has some impact on how well innovation is allowed to growonce incepted, i.e. some system might squash innovation.

### Communication

SNAFU Principle: "True communication is possible only between equals, because inferiors are more consistently rewarded for telling their superiors pleasant lies than for telling the truth."

Creative teamwork utterly depends on true communication and is thus very seriously hindered by the presence of power relationships.

The SNAFU principle predicts in authoritarian organizations a progressive disconnect between decision-makers and reality, as more and more of the input to those who decide tends to become pleasant lies.

--- Interesant but not pertinent for a DAO ---

### Good Projects

Necessity is the mother of invention.

Perfection (in design) is achieved not when there is nothing more to add, but when there is nothing more to take away.

Stay small, do one thing well.
Keep it modular.

### Good Programmers

Good programmers know what to write.
Great ones know what to rewrite (and reuse).

### Traditional Management Style

There exists OS communities that survived for a very long time without any traditional incentive organization.

What, if anything, is the tremendous overhead of conventionally-managed development actually buying us?

It certainly doesn't include reliable execution by deadline, or on budget, or to all features of the specification; it's a rare 'managed' project that meets even one of these goals, let alone all three.

A legally accountable person for the produced software? Not even: licenses are written in a way that prevents even that.
And you don't want to be in a lawsuit in any case.
You want software that works.

Has five functions:

1. Define goals and keep everybody pointed in the same direction.
2. Monitor and make sure crucial details don't get skipped.
3. Motivate people to do boring but necessary drudgework.
4. Oganize the deployment of people for best productivity.
5. Marshal resources needed to sustain the project.

Under OS development model:

1. ???
2. Peer reviews is probably the most effective way to ensure no detail is forgotten.
3. In OS, no need to motivate people: if they contribute then they are motivated.
  Best way to keep the community motivated is to make progress.
  The boring problems will also get done in the OS ecosystem if a person finds them interesting and decides to solve them or if soneone finds a way to circumvent them.
4. OS has been successful partly because its culture only accepts the most talented 5%.
  Managers spend most of their time organizing the 95% remaining percents.
5. Most resource marshalling is defensive, i.e. keep the people/offices that you obtained assigned to your project.
  Under OS, people bring their own resources to the table.
  The only really limited resources in the Internet era is skilled attention time.
