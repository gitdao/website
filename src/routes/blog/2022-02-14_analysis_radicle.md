---
title: Exploratory Deep-Dive in Radicle
date: 2022-02-14
categories:
- git
- exploration
---

> "At the core of the open source ethos is the idea of liberty. Open source is about inverting power structures and creating access and opportunities for everyone."
> 
> — GitHub employees' letter to GitHub

## What It Is

Open Source and decentralized GitHub:

- Code collaboration network
- Open Source protocols
- Peer-to-peer/decentralized
- Local by default: you only see content from peers you track and projects you liked
- Designed for Bazaar style development: no single view on the code, no official repo.
- Governance on the blockchain through RAD

## Code Sharing

Happens through a peer-to-peer protocol called Radicle Link that uses gossip.
Each network participant will keep a copy and disseminate the code they are interested in by keeping local copies.

## Organizations/DAOs

Radicle provides a way to build "Orgs" which are Gnosis Safes, i.e. MultiSig wallets.
Those Orgs can "anchor" projects, i.e. they are an on chain agreement between the Org owners about the state of a project's source.
They allow to pin a specific commit as being the current state of a given branch.

## Radicle and Ethereum

See [article](https://radicle.xyz/blog/integrating-with-ethereum.html).

Ethereum functionalities are an opt-in plugin.
Enables connecting to DAOs.

Enables recurrent funding.
Smart contract: [0xEc5bfca987C5FAA6d394044793f0aD6C9A85Da76](https://etherscan.io/address/0x37723287Ae6F34866d82EE623401f92Ec9013154)

Radicle has a governance token called RAD.

## Radicle Link

Peer-to-peer gossip protocol with generic version control backend (currently only supports git).

Disseminate git repos with gossip-based local replication.

### Identity

Two types:

- Personal
- Projects: `rad:git:$HASH[/$PATH]` which resolves to a project named `$HASH.git[/$PATH]` on Radicle

### Project

A project is the union of:

- Source ode
- Issues
- Proposed changes

It is encoded as a git repository.
I.e. the git repo will also contain social artifacts.

## Backed By

Radicle is backed by a Swiss non-profit Foundation.

## Used By

- [The Graph Improvement Proposal (GIP)](https://thegraph.com/blog/graph-radicle)
- [Synthetix](https://blog.synthetix.io/synthetix-repo-mirroring-on-radicle/)
