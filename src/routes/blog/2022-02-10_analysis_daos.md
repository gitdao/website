---
title: Exploratory Deep-Dive in DAOs
date: 2022-02-10
categories:
- DAO
- exploration
---

## Sources Included in this Document

- [ ] The DAO Whitepaper
- [ ] Aragon website
- [ ] Colony Whitepaper
- [ ] Colony website
- [ ] Snapshot
- [x] [Kraken Report on DAO](https://kraken.docsend.com/view/kfzxp6qqaqnqyue6)
- [x] [Alexandria's article on voting](https://coinmarketcap.com/alexandria/article/blockchain-elections-and-metaverse-voting-is-it-possible)
- [x] Aragon whitepaper
- [x] Gnosis Safe

## DAOs History

The first ever DAO was called "The DAO", constituted in 2016.
It pooled \$150M and its goal was to fund other projects on the blockchain.
It became famous because it was suffering from the reintrant call problem and \$70M dollars were stolen.

The developers decided to hard fork the network to erase the transaction in which the attacker stole the funds from the smart contract.
The hard fork was completed at block 1'920'000.
All miners that did not transition to the new Ethereum network now form what is called Ethereum Classic.

Since then, contract auditing is a thing.

Originally DAOs were mainly venture funds.
Now many of them coordinate to advance a mission like MolochDAO.
Some of them noiw also form with the goal of acquiring and running real world assets like ConstitutionDAO that raised \$47M in an attempt to purchase an original copy of the US Constitution at a Sotheby's auction.

## Famous DAOs

- MolochDAO
- Aragon
- Colony DAO
- Tribute DAO
- PleasrDAO, promotes creativity and NFTs
- Gnosis DAO
- Gitcoin

## What Makes a DAO

- Voting process
- Token model/Tokenomics
- Exit mechanism

## Moloch DAO

Constituted February 2019.
Distribute funds to project that contribute to Ethereum 2.0.

## Aragon DAO

Has a full suite of Open Source tools for building DAOs.

- Aragon Client: A user interface to create DAOs.
- Aragon Voice: Fee-less system to vote on blockchain.
- Vocdini: Censorship-resistant anonymous voting protocol.

### Aragon Whitepaper

Decentralized court solution: To arbitrate human conflicts beyond what can be reasonably coded in a smart contract.
Upgrade mechanism: Facilitate upgrades of smart contracts.

Aragon core: A dApp for creating and managing organizations of any kind: companies, Open Source projects, NGOs, etc.
It implements:

- Bylaws that define user permission, i.e. who can perform an action
- A governance system to make decisions
- A capital system for issuing and controlling tokens
- An accounting system to manage funds.

Aragon organizations are extensible through thrid party modules that interact with the org's smart contract.

Requirements of an org:

- Identity: Establish identity before interacting
- Ownership of the organization, e.g. through shares
- Voting
- Capital
- Rewarding mechanisms
- etc.

Integrated Keybase as identity provider.

Aragon Network: A network of DAOs coordinated from a set of smart contracts.
The network makes it possible for DAO to send funds to each others.
The network takes some fees to finance some services like:
- Development of the network's smart contracts
- Decentralized court with the power of freezing organization after a vote
- A contract upgradeablity feature to upgrade all Aragon Core contracts, with a bug-bounty program.

## Gnosis Safe

A MultiSig Wallet contract that requires M of N people to approve a transaction before it is sent.
