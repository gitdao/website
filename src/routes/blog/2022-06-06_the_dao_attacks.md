---
title: Attacks Against the DAO
date: 2022-06-06
---

## Attacks

URL: https://hackingdistributed.com/2016/05/27/dao-call-for-moratorium/

### No Incentive to Vote No

The DAO prevents people from spliting from the DAO once the have voted until the outcome of the vote is known.
If you think that the vote will have an negative expected value, you have two options:

- Split from the DAO immediately without taking any risks.
- Vote no and hope that the outcome of the vote is indeed no.

This creates a general bias towards the Yes for any proposal, which is based for funding systems based on the perceived sentiment of a crowd like the DAO.

### Staling Attacks

Prevents an investor from getting its token back when splitting from the DAO.

### Ambush Attacks

Create a obviously adversarial proposal to send funds from the DAO to yourself.
Some people will vote against, but this might not be much.
Now assume you are a whale which a very large share of the voting power.
Wait until the last moment, then overpower the few negative votes by voting yes.
The community will not have time to react against such an attack.

### The Token Raid

Attack by a whale to lower the value of DAO tokens, either to buy them at a discount or to win money through short options.

### The `extraBalance` Attack

A whale wins by incentivizing people to split from the DAO, thus increasing the value of DAO tokens.

### The Split Majority Takeover Attack

50% of the DAO token holders propose to award 100% of the DAO funds to themselves, thus robing the other 50%.

### The Concurrent Tie-Down Attack

Trap funds of investors in the DAO by creating a vote with a long period.
Ceate an adversarial vote with a very short voting period afterwards to be certain that you can benefit from the tokens of the trapped investors.

### Independence Assumption

Some proposals might not be independent.
They could be antagonistic: the success of one will imply the failure of the other.
Alternatively, they could be synergistic: the success of one will improve the other.
Investors have no way to provide complex preferences.

## Fixes

### Post-vote Grace Period

After vote and before the proposal is actually funded, people can withdraw their funds if they want to.

### 
