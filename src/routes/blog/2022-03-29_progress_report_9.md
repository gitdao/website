---
title: Progress Report 9
date: 2022-03-29
categories:
- progress-report
---

## Future Objectives

- Implement drips and donations
- Build Merge Request interface for Radicle?
- Propose GitDAO as alternative DAO option inside Radicle

## TODO

- [ ] Cleanup website
- [ ] Implement challenges
- [ ] Link contract ABI correctly
