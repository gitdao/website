---
title: Progress Week 2
date: 2022-02-01
categories:
- progress-report
---

## Website Improvements

- Created a blog in Svelte with support for the following functionalities (https://joshcollinsworth.com/blog/build-static-sveltekit-markdown-blog):
  - Blog articles in markdown through [mdsvex](https://mdsvex.com/)
  - Blog categories and sorting based on dates
  - Optimied images with [Svelte Image](https://github.com/matyunya/svelte-image)
  - Material Icons with [Svelte Material Icons](https://github.com/ramiroaisen/svelte-material-icons?ref=madewithsvelte.com)
  - Support for math through Mathjax (at runtime; compile-time would have been better, but the plugins did not seem to work)
  - Support for GitHub flavored markdown through [remark-gfm](https://github.com/remarkjs/remark-gfm)
  - Anchors for blog titles

## Open Source

- Read "The Cathedral and the Bazaar"
- Summarized "The Cathedral and the Bazaar" in my [notes](./2022-02-04_analysis_open_source).
- Started extracting from my summary of "The Cathedral and the Bazaar" some pertinent elements for the DAO.
