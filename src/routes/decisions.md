<script>
  import Decision from "../components/decisions/Decision.svelte";
  import DecisionReason from "../components/decisions/DecisionReason.svelte";

  let decisions = [
    {
      text: "Build an opinionated DAO.",
      date: "2022-02-08",
      reason: `
        Not sure what the opinion will be, but I won't try to fit all use case.
        Instead I will fit one use case well.
        DAOs are still in there infancy.
        Opinionated is better at this stage as it is clearer for users what the use is.
      `,
    },
    {
      text: `
        No classes of citizens.
        I.e. let's try to keep everyone equal and let's not use statuses/roles.
      `,
      date: "2022-02-08",
      reason: `
        SNAFU: "True communication is possible only between equals, because inferiors are more consistently rewarded for telling their superiors pleasant lies than for telling the truth."
        Open Source's success is most probably, at least in part, due to its anarchical organization.
        We want to keep this property.
      `,
    },
  ];
</script>

# Decisions

## DAO

<ul>
  {#each decisions as decision}
    <li>
      <Decision date={decision.date}>
        {decision.text}
        <DecisionReason>
          {decision.reason}
        </DecisionReason>
      </Decision>
    </li>
  {/each}
</ul>