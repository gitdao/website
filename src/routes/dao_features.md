# DAO Features

## Governance Tokens

A way to reward contributions.

- Mint and give governance tokens, i.e. NFTs, to developers that get a patch anchored.
- The governance tokens disolve themselves after a preset duration.
  That's why they are NFTs: each token has its own duration.
- Governance tokens cannot be exchanged, nor divided, they can be burned sooner than their set expiration date though.

Normalized power of an account:
  The sum of the values of the governance token that it owns divided by the total sum of all governance token's values.

What are governance tokens good for?

- Governance power: An account has a governance power equal to its normalized power.
- Monetary rewards: The account will receive an amount of drips proportional to its normalized power.

## Donations and Incomes

Donations and incomes are split according to a ratio fixed a priori between:

- Rewards
- Stash
- Drips

**Rewards** is the money that is given to the developers of the project proportionaly to their normalized power.

**Stash** is the money that is sent to the DAO reserve which can be used to fund other project, invest in something, or reward developers when they contribute particular features.

**Drips** is the money that is further sent to other projects' DAO, for example to the DAO of Open Source dependencies used by the current project.

## What Happens When Nobody Owns any Governance Tokens Anymore?

The project becomes dormant.
Any one can come and claim it.
That person will then receive some governance tokens.

## Voting System

What voting system to use?

Required features:

- Prevent voter fatigue
- Fast and efficient
- Malevolence resistance

## Rewarding Scheme

How should PR be rewarded?
How many governance token to mint and give away?
