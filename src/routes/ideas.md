# Ideas

## Rewards

People contribute to Open Source for different reasons.

Some might contribute for the money, some might do it for other reasons.
DAO should probably reward people not only with money, but through other means too.

NFT for doing a contribution to the project.
Gamification.
NFT that you can sell or stake.
When staking, if you have enough, you can convert to another type of NFT that has more value.

- Monetary, through an ERC20
- Social parts of the DAO: Bet on the future success of the project (like MolochDAO), retroactive-funding
- NFT / Badges: Recognition

Integrate with Radicle [Drip](https://www.drips.network/) protocol which allows receiving and redistributing funds.

## PR Staking

Staking on PR ?

What to stake:

- Real money?
- Governance Tokens?
- Social parts?

## Security

Is the DAO resistant in the face of adversarial agents?

## Roles

OS projects generally have a coordinator, e.g. Linus Torvald for Linux and the BDFL of the Python project until 2008.
Should we/How to emulate this role in the GitDAO?
