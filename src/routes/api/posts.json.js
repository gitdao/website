// API Endpoint that returns all the posts in this blog (and their metadata) as a JSON object.

export const get = async () => {

  // List all files that might contain blog articles
  const allPostFiles = Object.entries(
    Object.assign(
      {},
      import.meta.glob('../blog/*.svx'),
      import.meta.glob('../blog/*.md'),
    )
  );

  // Map each file found to its metadata
  const allPosts = await Promise.all(
    allPostFiles.map(async ([path, resolver]) => {
      const { metadata } = await resolver()
      const postPath = path.slice(2).replace(/\.[^/.]+$/, "")

      return {
        meta: metadata,
        path: postPath,
      }
    })
  )

  const sortedPosts = allPosts.sort((a, b) => {
    return new Date(b.meta.date) - new Date(a.meta.date)
  })

  return {
    body: sortedPosts
  }
}