import { writable } from "svelte/store";
import { getUnixTimestamp } from "../utils/time";

// TODO Support all types of tokens, not just the linear ones.
class Token {
  constructor({
    id,
    owner,
    decreasingPower,
    asymptoticPower,
    mintInstantInSeconds,
    halfLifeInSeconds
  }) {
    this.id = id;
    this.owner = owner;
    this.decreasingPower = decreasingPower;
    this.asymptoticPower = asymptoticPower;
    this.mintInstantInSeconds = mintInstantInSeconds;
    this.halfLifeInSeconds = halfLifeInSeconds;
  }

  getCurrentPower() {
    const now = getUnixTimestamp();
    const elapsedTimeSinceMintInstantInSeconds = now - Math.floor(this.mintInstantInSeconds.getTime() / 1000);
    const durationToAsymptoticPowerInSeconds = 2 * this.halfLifeInSeconds;

    let durationLeftUntilAsymptoticPower;
    if (elapsedTimeSinceMintInstantInSeconds >= durationToAsymptoticPowerInSeconds) {
        durationLeftUntilAsymptoticPower = 0;
    } else {
        durationLeftUntilAsymptoticPower = durationToAsymptoticPowerInSeconds - elapsedTimeSinceMintInstantInSeconds;
    }

    return Math.round(
      (this.asymptoticPower + this.decreasingPower * durationLeftUntilAsymptoticPower / durationToAsymptoticPowerInSeconds)
      * 10
    ) / 10;
  }

  static parseFromArgs(args) {
    return new Token({
      id: args[0],
      owner: args[1],
      decreasingPower: args[2].toNumber(),
      asymptoticPower: args[3].toNumber(),
      mintInstantInSeconds: new Date(args[4] * 1000),
      halfLifeInSeconds: args[5].toNumber()
    })
  }
}

export let governanceTokensById = writable(new Map());
export let governanceTokensByOwner = writable(new Map());

export async function updateGovernanceTokenStores(gitDaoContract) {
  const tempById = new Map();
  const tempByOwner = new Map();

  if (gitDaoContract) {
    let allGovernanceTokensFilter = gitDaoContract.filters.TimeLimitedGovernanceTokenMinted();
    const tokensRaw = await gitDaoContract.queryFilter(allGovernanceTokensFilter);

    for (const tokenData of tokensRaw) {
      const token = Token.parseFromArgs(tokenData.args);

      const id = token.id.toString();
      const owner = token.owner.toString();

      tempById.set(id, token);

      if (!tempByOwner.get(owner)) {
        tempByOwner.set(owner, [])
      }
      tempByOwner.get(owner).push(token);
    }
  }

  governanceTokensById.set(tempById);
  governanceTokensByOwner.set(tempByOwner);
}
