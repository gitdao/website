import { writable } from 'svelte/store';

class Vote {

  constructor({
    voter,
    proposalId,
    tokenIds,
    power,
    isInFavor,
  }) {
    this.voter = voter;
    this.proposalId = proposalId;
    this.tokenIds = tokenIds;
    this.power = power;
    this.isInFavor = isInFavor;
  }

  static parseFromArgs(args) {
    return new Vote({
      voter: args[0],
      proposalId: args[1],
      tokenIds: args[2],
      power: args[3].toNumber(),
      isInFavor: args[4]
    })
  }
}

export let votesByProposalId = writable(new Map());

export async function updateVoteStore(gitDaoContract) {
  const votes = new Map();

  if (gitDaoContract) {
    let allVotesFilter = gitDaoContract.filters.VoteCast();
    const votesRaw = await gitDaoContract.queryFilter(allVotesFilter);

    for (const voteData of votesRaw) {
      const args = voteData.args;
      const key = args[1].toString(); // The proposal ID
      const vote = Vote.parseFromArgs(args)
      if (!votes.get(key)) {
        votes.set(key, []);
      }
      votes.get(key).push(vote);
    }
  }

  votesByProposalId.set(votes)
}
