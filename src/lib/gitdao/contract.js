import { ethers } from "ethers";
import { defaultEvmStores } from "svelte-ethers-store";

import linearGitDaoArtifact from "/static/compiled-contracts/governance/LinearGitDao.sol/LinearGitDao.json"

import { updateProposalStore } from "./proposals";
import { updateGovernanceTokenStores } from "./governance-tokens";
import { updateVoteStore } from "./votes";

const linearGitDaoAbi = linearGitDaoArtifact.abi;

export function connectToGitDaoContract(gitDaoAddress) {
  let addressIsInvalid;
  if (ethers.utils.isAddress(gitDaoAddress)) {
    addressIsInvalid = false;
    defaultEvmStores.attachContract("gitDao", gitDaoAddress, linearGitDaoAbi);
  } else {
    addressIsInvalid = true;
  }

  return addressIsInvalid;
}

export function updateGitDaoData(gitDaoContract) {
  if (!gitDaoContract) {
    return
  }
  updateProposalStore(gitDaoContract);
  updateGovernanceTokenStores(gitDaoContract);
  updateVoteStore(gitDaoContract);
}
