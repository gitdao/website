import { writable } from 'svelte/store';

export const ProposalState = {
  Pending: "pending",
  VotingOpened: "voting_opened",
  VotingElapsed: "voting_elapsed",
  Succeeded: "succeeded",
  Defeated: "defeated",
  Canceled: "canceled"
}

class Proposal {

  constructor({
    id,
    createdBy,
    createdAt,
    proposedCommitMultihash,
    votingOpensAt,
    votingClosesAt
  }) {
    this.id = id;
    this.createdBy = createdBy;
    this.createdAt = createdAt;
    this.proposedCommitMultihash = proposedCommitMultihash;
    this.votingOpensAt = votingOpensAt;
    this.votingClosesAt = votingClosesAt;

    // TODO Support other states: Succeeded and Failed, requires parsing more events and merging the data
    let now = new Date();
    let state = ProposalState.Pending;
    if (votingOpensAt <= now && now < votingClosesAt) {
      state = ProposalState.VotingOpened;
    } else if (votingClosesAt <= now) {
      state = ProposalState.VotingElapsed;
    }
    this.state = state;
  }

  static parseFromArgs(args) {
    return new Proposal({
      id: args[0],
      createdBy: args[1],
      createdAt: new Date(args[2] * 1000),
      proposedCommitMultihash: args[3],
      votingOpensAt: new Date(args[4] * 1000),
      votingClosesAt: new Date(args[5] * 1000)
    })
  }
}

export let proposalsById = writable(new Map());

export async function updateProposalStore(gitDaoContract) {

  const proposals = new Map();

  if (gitDaoContract) {
    let allProposalsFilter = gitDaoContract.filters.ProposalCreated();
    const proposalsRaw = await gitDaoContract.queryFilter(allProposalsFilter);

    for (const proposalData of proposalsRaw) {
      const args = proposalData.args;
      proposals.set(args[0].toString(), Proposal.parseFromArgs(args));
    }
  }

  proposalsById.set(proposals)
}
