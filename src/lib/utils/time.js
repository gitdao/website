// Taken in part from https://muffinman.io/blog/javascript-time-ago-function/

const DAY_IN_MS = 86400000; // 24 * 60 * 60 * 1000

const MONTH_NAMES = [
  "January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

export function timeAgo(dateParam) {
  if (!dateParam) {
    return null;
  }

  const date = typeof dateParam === "object" ? dateParam : new Date(dateParam);
  const hours = date.getHours();
  const minutes = date.getMinutes();

  const now = new Date();
  const yesterday = new Date(now - DAY_IN_MS);
  const tomorrow = new Date(now + DAY_IN_MS);

  const secondsOffset = Math.round((date - now) / 1000);
  const minutesOffset = Math.round(secondsOffset / 60);

  const isToday = now.toDateString() === date.toDateString();
  const isYesterday = yesterday.toDateString() === date.toDateString();
  const isTomorrow = tomorrow.toDateString() === date.toDateString();

  if (secondsOffset <= 0) {
    // Past timestamps
    if (secondsOffset >= -5) {
      return "just now";
    } else if (secondsOffset >= -60) {
      return `${-secondsOffset} seconds ago`;
    } else if (secondsOffset >= -90) {
      return "about a minute ago";
    } else if (minutesOffset >= -60) {
      return `${-minutesOffset} minutes ago`;
    } else if (isToday) {
      return `today at ${hours}:${minutes}`;
    } else if (isYesterday) {
      return `yesterday at ${hours}:${minutes}`;
    }
  } else {
    // Future timestamps
    if (secondsOffset <= 5) {
      return "in a few seconds";
    } else if (secondsOffset <= 60) {
      return `in ${secondsOffset} seconds`;
    } else if (secondsOffset <= 90) {
      return "in about a minute";
    } else if (minutesOffset < 60) {
      return `in ${minutesOffset} minutes`;
    } else if (isToday) {
      return `today at ${hours}:${minutes}`;
    } else if (isTomorrow) {
      return `tomorrow at ${hours}:${minutes}`;
    }
  }

  const day = date.getDate();
  const month = MONTH_NAMES[date.getMonth()];
  const year = date.getFullYear();
  return `on the ${day}th of ${month} ${year} at ${hours}:${minutes}`;
}

export function getUnixTimestamp() {
  return Math.floor(Date.now() / 1000);
}
